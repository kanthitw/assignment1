package tw51.gameoflife;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GameOfLifeTest {

    private Set<Cell> inputCells() {
        Cell cells[] = new Cell[3];
        cells[0] = Cell.createCell(1, 1);
        cells[1] = Cell.createCell(1, 0);
        cells[2] = Cell.createCell(1, 2);
        Set<Cell> inputCoordinates = new HashSet<>();
        for (int i = 0; i < cells.length; i++) {
            inputCoordinates.add(cells[i]);
        }
        return inputCoordinates;
    }

    private Set<Cell> aliveCells() {
        Cell aliveCells[] = new Cell[3];
        aliveCells[0] = Cell.createCell(1, 1);
        aliveCells[1] = Cell.createCell(0, 1);
        aliveCells[2] = Cell.createCell(2, 1);
        Set<Cell> aliveCell = new HashSet<>();
        for (int i = 0; i < aliveCells.length; i++) {
            aliveCell.add(aliveCells[i]);
        }
        return aliveCell;
    }

    @Test
    public void liveCellWithTwoNeighboursShouldBeAlive() {
        Set<Cell> inputCoordinates = inputCells();
        GameOfLife game = GameOfLife.initialiseLiveCells(inputCoordinates);
        game.keepLiveCellsWithTwoOrThreeNeighboursAlive();
        assertEquals(true, game.isAlive(Cell.createCell(1, 1)));
    }

    @Test
    public void liveCellWithLessThanTwoNeighbourShouldBeDead() {
        Set<Cell> inputCoordinates = inputCells();
        GameOfLife game = GameOfLife.initialiseLiveCells(inputCoordinates);
        game.keepLiveCellsWithTwoOrThreeNeighboursAlive();
        assertEquals(false, game.isAlive(Cell.createCell(1, 0)));
    }

    @Test
    public void deadCellWithThreeLiveNeighbourShouldRevive() {
        Set<Cell> inputCoordinates = inputCells();
        GameOfLife game = GameOfLife.initialiseLiveCells(inputCoordinates);
        game.keepLiveCellsWithTwoOrThreeNeighboursAlive();
        game.reviveDeadCellsWithThreeNeighbours();
        assertEquals(true, game.isAlive(Cell.createCell(0, 1)));
    }

    @Test
    public void getNextGenerationLiveCells() {
        Set<Cell> inputCoordinates = inputCells();
        Set<Cell> aliveCells = aliveCells();
        GameOfLife game = GameOfLife.initialiseLiveCells(inputCoordinates);
        game.keepLiveCellsWithTwoOrThreeNeighboursAlive();
        assertTrue(game.checkNextGenerationCells(aliveCells));
    }

}
