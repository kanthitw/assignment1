package tw51.gameoflife;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class CellTest {
    private Cell[] getCells() {
        Cell cells[] = new Cell[3];
        cells[0] = Cell.createCell(1, 1);
        cells[1] = Cell.createCell(1, 0);
        cells[2] = Cell.createCell(1, 2);
        return cells;
    }

    @Test
    public void eachCellsAreDifferent() {
        assertTrue(Cell.createCell(2, 3).equals(Cell.createCell(2, 3)));
    }

    @Test
    public void checkLiveNeighbours() {
        Cell[] cells = getCells();
        Set<Cell> cell = new HashSet<>();
        for (int i = 0; i < cells.length; i++)
            cell.add(cells[i]);
        assertEquals(2, Cell.createCell(1, 1).getNumberOfLiveNeighbour(cell));
    }
}

