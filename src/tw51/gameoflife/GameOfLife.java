package tw51.gameoflife;

import java.util.*;

public class GameOfLife {

    private final Set<Cell> livingCells;
    private Set<Cell> nextGenerationLiveCells = new HashSet<>();

    private GameOfLife(Set<Cell> livingCells) {
        this.livingCells = livingCells;
    }

    public static GameOfLife initialiseLiveCells(Set<Cell> liveCells) {
        return new GameOfLife(liveCells);
    }

    void keepLiveCellsWithTwoOrThreeNeighboursAlive() {
        int numberOfLiveNeighbours;
        for (Cell alive : livingCells) {
            numberOfLiveNeighbours = alive.getNumberOfLiveNeighbour(livingCells);
            if ((numberOfLiveNeighbours == 2 || numberOfLiveNeighbours == 3)) {
                nextGenerationLiveCells.add(alive);
            }
        }
    }

    void reviveDeadCellsWithThreeNeighbours() {
        int numberOfLiveNeighbours;
        Set<Cell> deadCells = new HashSet<>();
        for (Cell alive : livingCells)
            deadCells = alive.getDeadNeighbour(livingCells);
        for (Cell dead : deadCells) {
            numberOfLiveNeighbours = dead.getNumberOfLiveNeighbour(livingCells);
            if (numberOfLiveNeighbours == 3) {
                nextGenerationLiveCells.add(dead);
            }
        }
    }

    public boolean checkNextGenerationCells(Set<Cell> expectedNextGenerationCells) {
        return expectedNextGenerationCells.containsAll(nextGenerationLiveCells);
    }

    public boolean isAlive(Cell cell) {
        return nextGenerationLiveCells.contains(cell);
    }

    private String getNextGenerationLiveCells() {
        return nextGenerationLiveCells.toString();
    }

    public static void main(String[] args) {
        System.out.println("GAME OF LIFE");
        System.out.println("Enter the number of input coordinates:");
        Scanner in = new Scanner(System.in);
        int numberOfInputs = in.nextInt();
        Set<Cell> inputCells = new HashSet<>();
        Cell[] cell = new Cell[numberOfInputs];
        for (int loop = 0; loop < numberOfInputs; loop++) {
            System.out.println("enter Coordinate" + (loop + 1) + " in xCoordinate,yCoordinate Format");
            String coordinate = in.next();
            String part[] = coordinate.split(",");
            int xCoordinate = Integer.parseInt(part[0]);
            int yCoordinate = Integer.parseInt(part[1]);
            cell[loop] = Cell.createCell(xCoordinate, yCoordinate);
            inputCells.add(cell[loop]);
        }
        GameOfLife game = new GameOfLife(inputCells);
        game.keepLiveCellsWithTwoOrThreeNeighboursAlive();
        game.reviveDeadCellsWithThreeNeighbours();
        System.out.println("Next Generation live cells:");
        System.out.println(game.getNextGenerationLiveCells());

    }
}
