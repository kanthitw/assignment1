package tw51.gameoflife;

import java.util.HashSet;
import java.util.Set;

public class Cell {
    private final int xCoordinate;
    private final int yCoordinate;

    private Cell(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public static Cell createCell(int xCoordinate, int yCoordinate) {
        return new Cell(xCoordinate, yCoordinate);
    }

    Set<Cell> getDeadNeighbour(Set<Cell> livingCells) {
        Set<Cell> deadCells = new HashSet<>();
        for (int neighbourXCoordinate = xCoordinate - 1; neighbourXCoordinate <= xCoordinate + 1; neighbourXCoordinate++) {
            for (int neighbourYCoordinate = yCoordinate - 1; neighbourYCoordinate <= yCoordinate + 1; neighbourYCoordinate++) {
                Cell neighbour = Cell.createCell(neighbourXCoordinate, neighbourYCoordinate);
                if (!livingCells.contains(neighbour))
                    deadCells.add(neighbour);
            }
        }
        return deadCells;
    }

    int getNumberOfLiveNeighbour(Set<Cell> livingCells) {
        int numberOfALiveNeighbour = 0;
        for (int neighbourXCoordinate = xCoordinate - 1; neighbourXCoordinate < xCoordinate + 2; neighbourXCoordinate++)
            for (int neighbourYCoordinate = yCoordinate - 1; neighbourYCoordinate < yCoordinate + 2; neighbourYCoordinate++) {
                if (neighbourXCoordinate != xCoordinate || neighbourYCoordinate != yCoordinate) {
                    Cell neighbour = Cell.createCell(neighbourXCoordinate, neighbourYCoordinate);
                    numberOfALiveNeighbour = incrementLiveNeighbourCount(livingCells, numberOfALiveNeighbour, neighbour);
                }
            }
        return numberOfALiveNeighbour;
    }


    private int incrementLiveNeighbourCount(Set<Cell> livingCells, int numberOfALiveNeighbour, Cell neighbour) {
        if (livingCells.contains(neighbour)) {
            numberOfALiveNeighbour++;
        }
        return numberOfALiveNeighbour;
    }


    @Override
    public int hashCode() {
        int result = xCoordinate;
        result = 31 * result + yCoordinate;
        return result;
    }

    @Override
    public String toString() {
        return "(" + xCoordinate + "," + yCoordinate +
            ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return xCoordinate == cell.xCoordinate && yCoordinate == cell.yCoordinate;
    }
}
